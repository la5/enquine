#include <stdio.h>

//--- Add Enquine ---
    
#define EnQAddStr "//--- Add Enquine ---"
#define EnQBegStr "//--- Beg Enquine ---"
#define EnQEndStr "//--- End Enquine ---"
#define EnQCodStr "//--- Add EnqCode ---"

// Find string2 at beginning of string1
static int EnqNeedlein(char *s1, char *s2) // Haystack, Needle
{
    while(*s1 && (*s2 && (*s2 == *s1) || (*s2=='.'))) {
        s1++;s2++;
    }

    return(*s2 ? 0 : 1);
}

#define EnqEscChar(c) (c=='\\' || c=='\"')

static char *EnqEscSrc(char *inbuf, char *outbuf)
{
    char *pi=inbuf, *po=outbuf;
    
    while(*pi) {
        if(EnqEscChar(*pi)) {
            *po='\\';
            po++;
        }
        *po=*pi;
        pi++;po++;
    }
    *po='\0';
    
    return(outbuf);
}

static char *stripeol(char *buf)
{
    for(char *p=buf;*p;p++) { *p=*p=='\n' ? '\0' : *p; }
    return(buf);
}

#define MAX_ENQ_BUFLEN 3072 // Arbitrary limit on line length. 

static char *EnqTopcodestrs[]={
    "",
    "#define SRC_ENQUINED",
    "",
    ".",".$"
};

static int EnqWriteTop(FILE *outfile, int writelevel)
{
    char **p=EnqTopcodestrs;
    if(writelevel==0) {
        fprintf(outfile, "%s\n", EnQBegStr);
        for(p=EnqTopcodestrs;**p != '.';p++) {
            fprintf(outfile, "%s\n", *p);
        }
    } else {
        if(writelevel==1) {
            char outbuf2[MAX_ENQ_BUFLEN*2+32];
            for(p=EnqTopcodestrs;**p != '.';p++) {
                fprintf(outfile, "\"%s\",\n", EnqEscSrc(*p, outbuf2));
            }
        } else {
            fprintf(outfile, "\n");
        }
    }
    
    return(1);
}

static int EnqWriteCode(char *infilename, FILE *outfile, int writelevel);

static int EnqWriteSrc(char *infilename, FILE *outfile, int writelevel)
{
    char inbuf[MAX_ENQ_BUFLEN+1];
    char outbuf[MAX_ENQ_BUFLEN*2+1];
    char outbuf2[MAX_ENQ_BUFLEN*4+1];
    char formatbuf[MAX_ENQ_BUFLEN+1];
    FILE *infile=fopen(infilename, "r");
    int retval=0;

    #define outformat "/* src */    \"%s\",\n"
    #define outformat2 EnqEscSrc(outformat,formatbuf)
    if(!infile) {
        printf("Failed to open file '%s' for read\n", infilename);
    } else {
        if(writelevel==1) {
            fprintf(outfile, "static char *EnQSource[]={\n");
            while(fgets(inbuf, MAX_ENQ_BUFLEN, infile)!=NULL) {
                if( EnqNeedlein(inbuf, EnQAddStr)) {
                    fprintf(outfile, "\"%s\",\n", "//--- Beg Enquine ---");
                    retval=EnqWriteCode(infilename, outfile, writelevel);
                } else if( EnqNeedlein(inbuf, EnQBegStr)) {
                    while(fgets(inbuf, MAX_ENQ_BUFLEN, infile)!=NULL){
                        if(EnqNeedlein(inbuf, EnQEndStr)) {
                    fprintf(outfile, "\"%s\",\n", "****Begin old****\n");
                            fprintf(stderr, "Writing code from Beg\n");
                            retval=EnqWriteCode(infilename, outfile, writelevel);
                            break;
                        }
                    }
                } else {
                    stripeol(inbuf);
                    EnqEscSrc(stripeol(inbuf), outbuf);
                    fprintf(outfile, "\"%s\",\n", outbuf);
                }
            }
            fprintf(outfile, "\".\"};\n");
        } else {
            EnqEscSrc(stripeol(inbuf), outbuf);
            EnqEscSrc(outbuf,outbuf2);
            fprintf(outfile, "\"static char *EnQSource[]={\",\n");
            fprintf(outfile, "\"%s\",\n", EnQCodStr);
            fprintf(outfile, "\"\\\".\\\"};\",\n");
            retval=1;
        }
    }
    
    return(retval);
}

static char *EnqEndcodestrs[]={
    "// Find string2 at beginning of string1",
    "static int QndNeedlein(char *s1, char *s2) // Haystack, Needle",
    "{",
    "    while(*s1 && (*s2 && (*s2 == *s1))) {",
    "        s1++;s2++;",
    "    }",
    "    return(*s2 ? 0 : 1);",
    "}",
    "",
    "#define QndCodStr \"//--- Add EnqCode ---\"",
    "",
    "#define QndEscChar(c) (c=='\\\\' || c=='\\\"')",
    "",
    "static char *QndEscSrc(char *inbuf, char *outbuf)",
    "{",
    "    char *pi=inbuf, *po=outbuf;",
    "    ",
    "    while(*pi) {",
    "        if(QndEscChar(*pi)) {",
    "            *po='\\\\';",
    "            po++;",
    "        }",
    "        *po=*pi;",
    "        pi++;po++;",
    "    }",
    "    *po='\\0';",
    "    ",
    "    return(outbuf);",
    "}",
    "",
    "#define MAX_QND_BUFLEN 3072 // Arbitrary limit on line length. ",
    "",
    "static void fEnqputsource(int writesrc, FILE *outfile)",
    "{",
    "    for(char **p=EnQSource;**p !='.' && **p!='.';p++) {",
    "        if( QndNeedlein(*p, QndCodStr) ) {",
    "            char **pp;",
    "            char outbuf[MAX_QND_BUFLEN*2+1];",
    "            for(pp=EnQSource;**pp !='.' && **pp!='.';pp++) {",
    "                fprintf(outfile, \"\\\"%s\\\",\\n\", QndEscSrc(*pp,outbuf));",
    "            }",
    "        } else {",
    "            fprintf(outfile, \"%s\\n\", *p);",
    "        }",
    "    }",
    "}",
        ".",".$"
};

static int EnqWriteEnd(FILE *outfile, int writelevel)
{
    char **p=EnqEndcodestrs;
    if(writelevel==0) {
        for(p=EnqEndcodestrs;**p != '.';p++) {
            fprintf(outfile, "%s\n", *p);
        }
        fprintf(outfile, "%s\n", EnQEndStr);
    } else {
        for(p=EnqEndcodestrs;**p != '.';p++) {
            char outbuf[MAX_ENQ_BUFLEN*2+1];
            EnqEscSrc(*p, outbuf);
            fprintf(outfile, "\"%s\",\n", outbuf);
        }
        fprintf(outfile, "\"%s\",\n", EnQEndStr);
    }
    
    return(1);
}

static int EnqWriteCode(char *infilename, FILE *outfile, int writelevel)
{
    int retval=0;
    FILE *infile=fopen(infilename, "r");
    
    if(!infile) {
        printf("Failed to open file '%s' for read\n", infilename);
    } else {
        retval=EnqWriteTop(outfile, writelevel);
        retval=retval && EnqWriteSrc(infilename, outfile, writelevel+1);
        retval=retval && EnqWriteEnd(outfile, writelevel);
        fclose(infile);
    }
    
    return(retval);
}

static int EnquineFile(char *infilename, FILE *infile, FILE *outfile)
{        
    char inbuf[MAX_ENQ_BUFLEN+1];
    char outbuf[MAX_ENQ_BUFLEN*2+1];
    int linenumber=0;
    int writelevel=0;
    int retval=0;

    while(fgets(inbuf, MAX_ENQ_BUFLEN, infile)!=NULL){
        if( EnqNeedlein(inbuf, EnQAddStr)) {
            retval=EnqWriteCode(infilename, outfile, writelevel);
            fprintf(stderr, "Writing code from Add\n");
        } else if( EnqNeedlein(inbuf, EnQBegStr)) {
            while(fgets(inbuf, MAX_ENQ_BUFLEN, infile)!=NULL){
                if(EnqNeedlein(inbuf, EnQEndStr)) {
                    fprintf(stderr, "Writing code from Beg\n");
                    retval=EnqWriteCode(infilename, outfile, writelevel);
                    break;
                } else {
//                    fprintf(outfile, "%s", inbuf);
                }
            }
        } else {
            fprintf(outfile, "%s", inbuf);
        }
        linenumber++;
    }
    
    return(retval);
}

int Enquine(char *infilename, char *outfilename)
{
    FILE *infile, *outfile;
    int retval=0;
    
    if( (infile = fopen(infilename, "r"))==NULL) {
        printf("failed to open input file '%s'\n", infilename);
    } else {
        FILE *outfile = *outfilename=='.' ? stdout : NULL;
        if(outfile==NULL) {
            if( (outfile = fopen(outfilename, "w"))==NULL) {
                printf("failed to open output file '%s'\n", outfilename);
            }
        }
        if(outfile!=NULL) {
            retval=EnquineFile(infilename, infile, outfile);
            if(outfile != stdout) {
                fclose(outfile);
            }
        }
    }
    return(retval);
}

static int testEnquine()
{
    int passed=1;
    passed = passed && Enquine("hello.c", ".");    
    printf("%s\n", passed ? "Pass" : "Failed");
    
    return(passed);
}

static void showuse()
{
    printf("  enquine  (Turn C code into quine) Bob Trower  2019-04-08\n");
    printf("                                              Version 0.00\n");
    printf("  Usage:   enquine [-opt] <sourcefile> [targetfile]       \n");
    printf("  Purpose: This command will add 'quine' code to a C      \n");
    printf("           source file.                                   \n");
    printf("           It looks for a string '%s'                     \n", EnQAddStr);
    printf("           and will replace that with quine code.         \n");
    printf("           If quine code exists, then it will replace     \n");
    printf("           code bracketed by the strings:                 \n");
    printf("           %s'                                            \n", EnQBegStr);
    printf("           %s'                                            \n", EnQEndStr);
    printf("  Options:                                                \n");
    printf("           -h Display help                                \n");
    printf("           -t self test.                                  \n");
    printf("           -g generate 'hello.c' file for test above.     \n");
    printf("           -s Display source code.                        \n");
    printf("  Example: enquine -h   << Display this help screen.      \n");
    printf("  Example: enquine -s prints source code                  \n");
    printf("  Note:    Generated hello.c file shows how to set up     \n");
    printf("           to enquine a file.                             \n");
    printf("  Release: 0.00.02, Mon Apr 08 2019 18:20:43:45, ANSI C   \n");   
}

static char *genhellostrs[] = {
    "#include <stdio.h>\n",
    "//--- Add Enquine ---\n",
    "#ifndef SRC_ENQUINED",
    "    #define showsource() printf(\"code not enquined.\\n\");",
    "#else",
    "    #define showsource() fEnqputsource(1,stdout)",
    "#endif\n",
    "int main(int argc, char **argv)",
    "{",
    "    showsource();",
    "}",
    "."
};

static void genhello()
{
    for(char **p=genhellostrs;**p != '.';p++) {
        printf("%s\n", *p);
    }
}

#ifndef SRC_ENQUINED
    #define showsource()
#else
    #define showsource() fEnqputsource(1,stdout)
#endif

int main( int argc, char**argv )
{
    int retcode=0;
    if(argc<2) {
        showuse();
    } else {
        if(argv[1][0]=='-') {
            int opt=argv[1][1];
            if(opt=='h') {
                showuse();
            } else if(opt=='s') {
                showsource();
            } else if(opt=='g') {
                genhello();
            } else if(opt=='t') {
                retcode=!testEnquine();
            }
        } else {
            printf("OK\n");
            if(argc>1) {
                char *infilename=argv[1];
                char *outfilename=argv[2]==NULL ? "." : argv[2];
                Enquine(infilename, outfilename);
            }
        }
    }
    
    return(retcode);
}
