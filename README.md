enquine  (Turn C code into quine) Bob Trower  2019-04-08
                                            Version 0.00

Usage:   enquine [-opt] <sourcefile> [targetfile]

Purpose: This command will add 'quine' code to a C source file. 
It looks for a string '//--- Add Enquine ---' and will replace that with quine code. 
If quine code exists, then it will replace code bracketed by the strings:
 //--- Beg Enquine ---'
 //--- End Enquine ---'

Options:
         -h Display help
         -t self test.
         -g generate 'hello.c' file for test above.
         -s Display source code.
Example: enquine -h   << Display this help screen.
Example: enquine -s prints source code
Note:    Generated hello.c file shows how to set up
         to enquine a file.

Release: 0.00.02, Mon Apr 08 2019 18:20:43:45, ANSI C